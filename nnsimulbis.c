/*
% Neural Network for Static models -- Simulation
%
% Usage 1: Output prediction                    - NOT COMPUTED HERE - 
%     Out=nnsimul(Wh,Wo,Inp)
%
% Usage 2: Output & standard deviation          - THE ONLY ONE COMPUTED HERE -
%     [Out,StdOut]=nnsimulbis(Wh,Wo,Inp,StdRes,CovW)
%
% Usage 3: Residuals, gradient & hessian        - NOT COMPUTED HERE -
%     [Sse,Gra,Hes]=nnsimulter(Wh,Wo,Inp,Out)
%
% Var     Size       Significance
% ----    ---------- -----------------------------------------
% Ni     (1 x 1)     Number of network inputs (without bias)
% Nh     (1 x 1)     Number of neurons in the hidden layer (without bias)
% No     (1 x 1)     Number of network outputs
% Nd     (1 x 1)     Number of experimental data points (observations)
% Nw     (1 x 1)     Total number of weights = (Ni+1)*Nh + (Nh+1)*No
%
% Wh     (Ni+1 x Nh) Weight matrix for the hidden layer (tanh transfer function)
% Wo     (Nh+1 x No) Weight matrix for the output layer (identity transfer function)
% x      (Nd x Ni)   Input data matrix. One row per observation.
% y      (Nd x No)   Output data matrix. One row per observation.
% stdout (Nd x No)   Estimated standard deviation of predicted output.
% stdres (1 x No)    Standard deviation of the residual (measurement) noise.
% CovW   (Nw x Nw)   Covariance matrix of network weights.
% Sse    (1 x No)    Sums of squares of the errors for each output.
% Gra    (Nw x No)   Gradients for each output: partial derivatives of the
%                    sums of squares of the errors with respect to each weight.
% Hes (Nw x Nw x No) Hessians for each output: approximate second order
%                    derivatives of the sums of squares of the errors
%                    with respect to the weights.

% (c) 1999 Ioan Cristian TRELEA
*/


#include "nnsimul.h"
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>


#define wh(a,b)         (wh[(a)+(ni+1)*(b)])
#define wo(a,b)         (wo[(a)+(nh+1)*(b)])
#define x(a,b)        (x[(a)+(nd)*(b)])
#define y(a,b)        (y[(a)+(nd)*(b)])
#define Stdout(a,b)   (Stdout[(a)+(nd)*(b)])
#define covw(a,b)       (covw[(a)+(nw)*(b)])
#define dOutdWh(a,b,c)  (dOutdW[(a)+(no)*(b)+(no)*(ni+1)*(c)])
#define dOutdWo(a,b,c)  (dOutdW[(no)*(ni+1)*(nh) + (a)+(no)*(b)+(no)*(nh+1)*(c)])
#define dOutdW(a,b)     (dOutdW[(a)+(no)*(b)])



/*
void ErrMsg(int ErrNmb, ...)
 { char ErrBuf[256];
   va_list arglist;
   va_start(arglist,ErrNmb);
   vsprintf(ErrBuf,ErrTbl[ErrNmb],arglist);
   va_end(arglist);
   mexErrMsgTxt(ErrBuf);
 }
*/

double nnsimulbisfh(double a)                      // Transfer function hidden layer
{ return 2.0/(1.0+exp(-2*a))-1;
}

double nnsimulbisdfh(double a)                     // Derivative transfer function hidden layer
{ return 1-a*a;
}

double nnsimulbisfo(double a)                      // Transfer function output layer
{ return a;
}

double nnsimulbisdfo(double a)                     // Derivative transfer function output layer
{ return 1; 
}




   // Computation usage 2: [y,StdOut]=nnsimulbis(wh,wo,x,StdRes,CovW)
   int nnsimulbis(double* wh, double* wo, double* x, double* stdres, double* covw, double* y, double* Stdout, int ni, int nd, int nh, int no, int nw) 

{

    int i;
    int ii,ih,io,id,iw,jw;
    double *Hid,Tmp,*dOutdW;

    // [Out,StdOut]=nnsimul(Wh,Wo,Inp,StdRes,CovW)
    Hid=malloc((nh+1)*sizeof(double));  // Place for hidden layer
    dOutdW=malloc(no*nw*sizeof(double)); // For 1 data point
    for (iw=0; iw<no*nw; iw++) dOutdW[iw]=0.0; // Ensure dOutdWo(io,ih,jo)=0, for io!=jo
        Hid[nh]=1.0;                   // Bias
        for (id=0; id<nd; id++)        // For all data points
        { // Compute Out
            for (ih=0; ih<nh; ih++)    // For all hidden neurons
            { Hid[ih]=wh(ni,ih);     // Bias
                for (ii=0; ii<ni; ii++) Hid[ih]+=x(id,ii)*wh(ii,ih);
                Hid[ih]=nnsimulbisfh(Hid[ih]);   // Transfer function
            }
            for (io=0; io<no; io++)    // For all outputs
            { y(id,io)=wo(nh,io);  // Bias
              for (ih=0; ih<nh; ih++) y(id,io)+=Hid[ih]*wo(ih,io);
              y(id,io)=nnsimulbisfo(y(id,io));   // Transfer function
            }
            // Compute dOutdW
            for (io=0; io<no; io++)    // For all outputs
            { for (ih=0; ih<nh; ih++)// For all hidden neurons, except bias
            { for (ii=0; ii<ni; ii++) // For all inputs, except bias
                dOutdWh(io,ii,ih)=x(id,ii)*nnsimulbisdfh(Hid[ih])*wo(ih,io)*nnsimulbisdfo(y(id,io));
                dOutdWh(io,ni,ih)=nnsimulbisdfh(Hid[ih])*wo(ih,io)*nnsimulbisdfo(y(id,io)); // For the bias
            }
                for (ih=0; ih<=nh; ih++)// For all hidden neurons, including bias
                    dOutdWo(io,ih,io)=Hid[ih]*nnsimulbisdfo(y(id,io));  
            }
            // Compute StdOut
            for (io=0; io<no; io++)    // For all outputs
            { Stdout(id,io)=stdres[io]*stdres[io];
                for (iw=0; iw<nw; iw++)// dOutdW(io,:)' * CovW *dOutdW(io,:)
                { Tmp=0.0;
                    for (jw=0; jw<nw; jw++) Tmp+=dOutdW(io,jw)*covw(jw,iw);
                    Stdout(id,io)+=Tmp*dOutdW(io,iw);
                }
                Stdout(id,io)=sqrt(Stdout(id,io));
            }
        }
    mxFree(dOutdW);
    mxFree(Hid);
      
    return 0;

 }


