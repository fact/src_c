/*
% Neural Network for Static models -- Simulation
%
% Usage 1: Output prediction                    - THE ONLY ONE COMPUTED HERE - 
%     Out=nnsimul(Wh,Wo,Inp)
%
% Usage 2: Output & standard deviation          - NOT COMPUTED HERE -
%     [Out,StdOut]=nnsimulbis(Wh,Wo,Inp,StdRes,CovW)
%
% Usage 3: Residuals, gradient & hessian        - NOT COMPUTED HERE -
%     [Sse,Gra,Hes]=nnsimulter(Wh,Wo,Inp,Out)
%
% Var     Size       Significance
% ----    ---------- -----------------------------------------
% Ni     (1 x 1)     Number of network inputs (without bias)
% Nh     (1 x 1)     Number of neurons in the hidden layer (without bias)
% No     (1 x 1)     Number of network outputs
% Nd     (1 x 1)     Number of experimental data points (observations)
% Nw     (1 x 1)     Total number of weights = (Ni+1)*Nh + (Nh+1)*No
%
% Wh     (Ni+1 x Nh) Weight matrix for the hidden layer (tanh transfer function)
% Wo     (Nh+1 x No) Weight matrix for the output layer (identity transfer function)
% Inp    (Nd x Ni)   Input data matrix. One row per observation.
% Out    (Nd x No)   Output data matrix. One row per observation.
% StdOut (Nd x No)   Estimated standard deviation of predicted output.
% StdRes (1 x No)    Standard deviation of the residual (measurement) noise.
% CovW   (Nw x Nw)   Covariance matrix of network weights.
% Sse    (1 x No)    Sums of squares of the errors for each output.
% Gra    (Nw x No)   Gradients for each output: partial derivatives of the
%                    sums of squares of the errors with respect to each weight.
% Hes (Nw x Nw x No) Hessians for each output: approximate second order
%                    derivatives of the sums of squares of the errors
%                    with respect to the weights.

% (c) 1999 Ioan Cristian TRELEA
*/


#include "nnsimul.h"
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>


#define wh(a,b)         (wh[(a)+(ni+1)*(b)])
#define wo(a,b)         (wo[(a)+(nh+1)*(b)])
#define x(a,b)          (x[(a)+(nd)*(b)])
#define y(a,b)          (y[(a)+(nd)*(b)])


//    char *ErrTbl[] = 
//   {
//  /*   0 */   "'wh' indicates  nh=%d hidden neurons, but 'wo' has %d rows",
//  /*   1 */   "'wh' indicates ni=%d inputs, but 'x' has %d columns",
//  /*   2 */   "'StdRes' should be 1 x %d, since No=%d.",
//  /*   3 */   "'CovW' should be %d x %d, since Nw=%d.",
//  /*   4 */   "'Out' should be %d x %d, since Nd=%d and No=%d."
//   };

 
   double nnsimulfh(double a)                      // Transfer function hidden layer
   { return 2.0/(1.0+exp(-2*a))-1;
   }

   double nnsimuldfh(double a)                     // Derivative transfer function hidden layer
   { return 1-a*a;
   }

   double nnsimulfo(double a)                      // Transfer function output layer
   { return a;
   }

   double nnsimuldfo(double a)                     // Derivative transfer function output layer
   { return 1; 
   }
 

   // Computation Usage 1:  y=nnsimul(wh,wo,x)
   int nnsimul(double* wh, double* wo, double* x, double* y, int ni, int nd, int nh, int no )
   
   {
   
     int i;
     int ii,ih,io,id,iw,jw;
   
     double *Hid;
  

     // Out=nnsimul(Wh,Wo,Inp)
     Hid=malloc((nh+1)*sizeof(double));      // Place for hidden layer
     Hid[nh]=1.0;                   // Bias
     for (id=0; id<nd; id++)        // For all data points
        { for (ih=0; ih<nh; ih++)    // For all hidden neurons
            { Hid[ih]=wh(ni,ih);     // Bias
              for (ii=0; ii<ni; ii++) Hid[ih]+=x(id,ii)*wh(ii,ih);
              Hid[ih]=nnsimulfh(Hid[ih]);   // Transfer function
            }
            for (io=0; io<no; io++)    // For all outputs
              { y(id,io)=wo(nh,io);  // Bias
                for (ih=0; ih<nh; ih++) y(id,io)+=Hid[ih]*wo(ih,io);
                y(id,io)=nnsimulfo(y(id,io));   // Transfer function
              }
        }
     free(Hid);
      
     return 0;
 
   }
  




