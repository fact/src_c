/* ==================================================================== */
/* Template toolbox_skeleton */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */
#ifndef __NNSIMUL_H__
#define __NNSIMUL_H__

#ifdef WIN32
#  ifdef FACT_EXPORTS
#    define FACT_API __declspec(dllexport)
#  else
#    define FACT_API __declspec(dllimport)
#  endif
#else
#  define FACT_API
#endif


FACT_API int nnsimul(double* wh, double* wo, double* x, double* y, int ni, int nd, int nh, int no );
FACT_API int nnsimulbis(double* wh, double* wo, double* x, double* stdres, double* covw, double* y, double* Stdout, int ni, int nd, int nh, int no, int nw );
FACT_API int nnsimulter(double* wh, double* wo, double* x, double* y, double* sse, double* gra, double* hes, int ni, int nd, int nh, int no, int nw );


#endif /* __NNSIMUL_H__ */

